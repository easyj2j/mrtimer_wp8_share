﻿using System;
using System.Windows.Input;

namespace MRTimer
{
    public class Command : ICommand
    {
        Func<object, bool> canExecuteDelegate;
        Action<object> executeDelegate;
        public event EventHandler CanExecuteChanged;

        public Command(Action<object> executeDelegate)
        {
            this.canExecuteDelegate = (e) => true;
            this.executeDelegate = executeDelegate;
        }

        public Command(Func<object, bool> canExecuteDelegate, Action<object> executeDelegate)
        {
            this.canExecuteDelegate = canExecuteDelegate;
            this.executeDelegate = executeDelegate;
        }

        public bool CanExecute(object parameter)
        {
            if (canExecuteDelegate != null) { return canExecuteDelegate(parameter); }
            else { return true; }
        }

        public void Execute(object parameter)
        {
            if (executeDelegate != null) { executeDelegate(parameter); }
        }

        public void RaiseCanExecuteChanged()
        {
            if(CanExecuteChanged != null) { CanExecuteChanged(this, EventArgs.Empty); }
        }
    }
}
