﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MRTimer.Resources;
using SQLite;
using System;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Navigation;
using Windows.Networking.Proximity;
using Windows.Phone.Speech.VoiceCommands;
using Windows.Storage;

/*
 * Two ways to connect to device:
 * 1) During app creation, an attempt is made to connect to last successful connection (no user involvement)
 *    using ConnectToLastConnectionAsync().
 * 
 * 2) If auto-connect at app creation fails or to change device, user can find & select
 *    desired device (peer) via TimerSettingsPage and connect using Timer_ConnectAsync(PeerInformation).
 */

namespace MRTimer
{
    public partial class App : Application
    {
        public static PhoneApplicationFrame RootFrame { get; private set; }

        public const string APPTITLE = "MRTimer";
        const string ISOLATED_STORAGE_FILE = "__ApplicationSettings";

        public string AppTitle { get; private set; }

        public TimerSettingsPage TimerSettings_Page { private get; set; }

        BluetoothConnection btConnection;
        public bool IsConnected { get; private set; }

        public SQLiteAsyncConnection DbConnection { get; private set; }
        const string dbName = "timer_scores.db";

        public ScoreViewModel ScoreVM { get; private set; }
        public ScoresViewModel ScoresVM { get; set; }

        string initialSortColumn = "Inserted";

        public App()
        {
            AppTitle = APPTITLE;

            ScoreVM = new ScoreViewModel();

            VoiceCommandService.InstallCommandSetsFromFileAsync(new Uri("ms-appx:///VoiceCommands.xml"));

            // BluetoothConnection
            btConnection = new BluetoothConnection();
            btConnection.ConnectingEvent   += Bluetooth_ConnectingEvent;
            btConnection.ConnectedEvent    += Bluetooth_ConnectedEvent;
            btConnection.NewScoreEvent     += Bluetooth_NewScoreEventAsync;
            btConnection.DisconnectedEvent += Bluetooth_DisconnectedEvent;

            ConnectToLastConnectionAsync();

            CreateDbAsync();    // create timer scores database if necessary

            ScoresVM = new ScoresViewModel();
            ScoresVM.PopulateScoreListAsync(DbConnection, initialSortColumn, false);
            
            #region Default initialization

            // Global handler for uncaught exceptions.
            UnhandledException += Application_UnhandledException;

            // Standard XAML initialization
            InitializeComponent();

            // Phone-specific initialization
            InitializePhoneApplication();

            // Language display initialization
            InitializeLanguage();

            // Show graphics profiling information while debugging.
            if (Debugger.IsAttached)
            {
                // Display the current frame rate counters.
                //Application.Current.Host.Settings.EnableFrameRateCounter = true;
                //MemoryDiagnostics.MemoryDiagnosticsHelper.Start(TimeSpan.FromMilliseconds(500), true);

                // Show the areas of the app that are being redrawn in each frame.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Enable non-production analysis visualization mode,
                // which shows areas of a page that are handed off to GPU with a colored overlay.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;

                // Prevent the screen from turning off while under the debugger by disabling
                // the application's idle detection.
                // Caution:- Use this under debug mode only. Application that disables user idle detection will continue to run
                // and consume battery power when the user is not using the phone.
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }
            #endregion
        }

        // Attempts to connect to last connected timer device.
        async Task ConnectToLastConnectionAsync()
        {
            try
            {
                string displayName;

                if ((IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>(
                    "LastConnectedDevice", out displayName)) && (displayName != null))
                {
                    PeerFinder.AlternateIdentities["Bluetooth:Paired"] = "";

                    // Throws if BT not enabled - no other way to check
                    var peers = await PeerFinder.FindAllPeersAsync();

                    foreach (PeerInformation peer in peers)
                    {
                        if (peer.DisplayName == displayName)
                        {
                            //PeerConnectedTo = peer;
                            await btConnection.ConnectToTimerAsync(peer).ConfigureAwait(false);
                            return;
                        }
                    }
                }
            }
            catch (Exception e) // TypeInitializationException
            {
                Debug.WriteLine("ConnectToLastConnectionAsync: " + Environment.NewLine + e.Message);
                /* user needs to manually connect */
            }
        }

        // Sets the path to the timer scores DB and creates the DB if it doesn't exist.
        async Task CreateDbAsync()
        {
            // Sets up the connection string but doesn't connect to the DB.
            // Not an async method; creates an async connection.
            DbConnection = new SQLiteAsyncConnection(
                Path.Combine(ApplicationData.Current.LocalFolder.Path, dbName), true);

#if DEBUG
            // Any existing DB is deleted when the app is uninstalled, which occurs
            // frequently while debugging. The project is set up so that during installation,   
            // VS copies a populated DB into the install folder where it is read-only.
            // This method copies the DB from the install folder to the local folder (IsolatedStorage).
            // NOTE: Any scores added to the DB are lost when the app is restarted due to the DB being overwritten.
            IsolatedStorageHelper.CopyFileFromInstallToLocalFolder(dbName, FileMode.Create);
#else
            try
            {
                // Connects to or creates the DB.
                await DbConnection.CreateTableAsync<Score>().ContinueWith((results) =>
                {
                    Debug.WriteLine("CreateTableAsync<Score>() successful");
                });
            }
            // TODO: These don't catch the above ex's - 3/30/16
            catch (FileNotFoundException ex)
            {
                Debug.WriteLine("Error creating/connecting to " + dbName + ": " + ex.Message);
            }
            catch (SQLiteException ex)
            {
                Debug.WriteLine("Error creating/connecting to " + dbName + ": " + ex.Message);
            }
#endif
        }

        // Create connection to device.
        // Called by TimerSettingsPage.ConnectToSelected_TapAsync.
        public async Task Timer_ConnectAsync(PeerInformation peer)
        {
            //PeerConnectedTo = peer;
            await btConnection.ConnectToTimerAsync(peer).ConfigureAwait(false);
        }

        // Set properties corresponding to voice commands 
        // that need to be processed by btConnection
        // after async connection completes successfully.
        // This isn't required for WP 8.1 Silverlight apps
        // but the BT connection is flaky for those.
        // Called by ScorePage.OnNavigatedTo.
        public void SetLatchPropsForVoice(bool isLatched)
        {
                btConnection.SendLatch = isLatched? true : false;
                btConnection.ReleaseLatch = isLatched ? false : true;
        }

        // Toggle timer display: latched or running.
        public void Timer_Latch(bool latch)
        {
            TimerCommand command = latch ? TimerCommand.LatchDisplay : TimerCommand.EnableDisplay; // 'e'

            btConnection.SendCommandAsync(command);
        }

        #region Event handlers

        // Navigate to AppSettings page from AppBar.
        public void AppBarAppSettings_Click(object sender, EventArgs e)
        {
            RootFrame.Navigate(new Uri("/AppSettingsPage.xaml", UriKind.Relative));
        }

        // Navigate to TimerSettings page from AppBar.
        public void AppBarTimerSettings_Click(object sender, EventArgs e)
        {
            RootFrame.Navigate(new Uri("/TimerSettingsPage.xaml", UriKind.Relative));
        }

        public void Bluetooth_ConnectingEvent(object sender, EventArgs e)
        {
            if (TimerSettings_Page != null)
            {
                TimerSettings_Page.SelectTimer.IsEnabled = false;
                TimerSettings_Page.PairedDevicesList.IsEnabled = false;
                TimerSettings_Page.ConnectToSelected.IsEnabled = false;
                TimerSettings_Page.ConnectToSelected.Content = "Connecting ...";
            }
        }

        public void Bluetooth_ConnectedEvent(object sender, BluetoothConnection.ConnectedEventArgs e)
        {
            IsConnected = true;
            AppTitle = e.DisplayName;

            // Save last successful connected device.
            IsolatedStorageSettings.ApplicationSettings["LastConnectedDevice"] = e.DisplayName;

            if (ScoreVM != null)
            {
                ScoreVM.IsConnected = true;
                ScoreVM.UpdateLatchButtonState();
            }

            if (TimerSettings_Page != null)
            {
                TimerSettings_Page.AppTitle.Text = "Connected to " + e.DisplayName;
                TimerSettings_Page.PairedDevicesList.Visibility = Visibility.Collapsed;
                TimerSettings_Page.ConnectToSelected.IsEnabled = false;
                TimerSettings_Page.ConnectToSelected.Content = "Connected";
                TimerSettings_Page.TriggerControl.IsEnabled = true;
                TimerSettings_Page.DelayControl.IsEnabled = true;
                TimerSettings_Page.CompetitionControl.IsEnabled = true;
            }

            // TODO: test voice commands after all the revisions
            //if (Score_Page != null)
            //{   // Need this test when using voice commands.
            //    if (ScorePage.NOTCONNECTED == Score_Page.LatchButton.Content.ToString())
            //    {
            //        Score_Page.LatchButton.Content = ScorePage.LATCHED;
            //        Score_Page.isLatched = false;
            //    }
            //    Score_Page.LatchButton.IsEnabled = true;
            //}
        }

        public async void Bluetooth_NewScoreEventAsync(object sender, BluetoothConnection.NewScoreEventArgs e)
        {
            // TODO: if ScoreVM.LatestScore == null is nullrefexc thrown?
            string userName = ScoreVM.LatestScore.Name ?? "B Lee";
            Score score = new Score(userName, e.Reaction, e.Motion, e.Response);

            // Update ScorePage
            ScoreVM.UpdateScore(score);
            
            if (e.Response == Score.ZERO) { return; }   // not a new score

            // Update DB - this sets score.ID
            // Since Score.ID is AutoIncrement PrimaryKey, InsertAsync sets ID to the id of the row inserted by sqlite.
            // 't' returns the # of rows inserted.
            await DbConnection.InsertAsync(score).ContinueWith((t) =>
            {
                if (t.Result != 1)
                {
                    MessageBox.Show("Bluetooth_NewScoreEventAsync -> InsertAsync error: # of rows inserted = " + t.Result);
                }
                Debug.WriteLine("New score ID: {0}", score.ID);
            });

            // Update ScoresPage
            ScoresVM.UpdateScores(score);
        }

        public void Bluetooth_DisconnectedEvent(object sender, EventArgs e)
        {
            IsConnected = false;
            AppTitle = APPTITLE;

            if (ScoreVM != null)
            {
                ScoreVM.IsConnected = false;
                ScoreVM.UpdateLatchButtonState();
            }

            if (TimerSettings_Page != null)
            {
                TimerSettings_Page.AppTitle.Text = APPTITLE + " disconnected";
                TimerSettings_Page.SelectTimer.IsEnabled = true;
                TimerSettings_Page.ConnectToSelected.Content = "Disconnected";
                TimerSettings_Page.TriggerControl.IsEnabled = false;
                TimerSettings_Page.DelayControl.IsEnabled = false;
                TimerSettings_Page.CompetitionControl.IsEnabled = false;
            }
        }
        
        public void TimerSettings_TimerCommandEvent(object sender, TimerCommandEventArgs e)
        {
            if (null == btConnection) { return; }

            btConnection.SendCommandAsync(e.Category);
            btConnection.SendCommandAsync(e.Command);
        }

        // An app user was either added or deleted (AppSettingsPage).
        public void Settings_UserChange(object sender, AStringEventArgs e)
        {
            //ScoreVM.UserDeleted(e.TheString);
            //ScoreVM.UserChanged = true;
        }

        //public void Settings_NewUser(object sender, EventArgs e) {}

        //public void Settings_DeleteUser(object sender, EventArgs e) {}
        
        #endregion

        #region Phone lifecycle

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
        }

        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
        }

        // Code to execute if a navigation fails
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                Debugger.Break();
            }
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger.
                Debugger.Break();
            }
        }

        #endregion

        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;

        // Do not add any additional code to this method
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // Create the frame but don't set it as RootVisual yet; this allows the splash
            // screen to remain active until the application is ready to render.
            RootFrame = new PhoneApplicationFrame();
            RootFrame.Navigated += CompleteInitializePhoneApplication;

            // Handle navigation failures
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // Handle reset requests for clearing the backstack
            RootFrame.Navigated += CheckForResetNavigation;

            // Set startup page from code. 
            // Also must delete NavigationPage="Page.xaml" from WMAppManifest.xml <DefaultTask Name="_default" NavigationPage="Page.xaml" />
            //RootFrame.Navigate(new Uri("/ScorePage.xaml", UriKind.RelativeOrAbsolute));

            // Ensure we don't initialize again
            phoneApplicationInitialized = true;
        }

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        private void CheckForResetNavigation(object sender, NavigationEventArgs e)
        {
            // If the app has received a 'reset' navigation, then we need to check
            // on the next navigation to see if the page stack should be reset
            if (e.NavigationMode == NavigationMode.Reset)
                RootFrame.Navigated += ClearBackStackAfterReset;
        }

        private void ClearBackStackAfterReset(object sender, NavigationEventArgs e)
        {
            // Unregister the event so it doesn't get called again
            RootFrame.Navigated -= ClearBackStackAfterReset;

            // Only clear the stack for 'new' (forward) and 'refresh' navigations
            if (e.NavigationMode != NavigationMode.New && e.NavigationMode != NavigationMode.Refresh)
                return;

            // For UI consistency, clear the entire page stack
            while (RootFrame.RemoveBackEntry() != null)
            {
                ; // do nothing
            }
        }

        private void InitializeLanguage()
        {
            try
            {
                RootFrame.Language = XmlLanguage.GetLanguage(AppResources.ResourceLanguage);
                FlowDirection flow = (FlowDirection)Enum.Parse(typeof(FlowDirection), AppResources.ResourceFlowDirection);
                RootFrame.FlowDirection = flow;
            }
            catch
            {
                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }

                throw;
            }
        }

    #endregion
    }

    public class AStringEventArgs : EventArgs
    {
        public string TheString { get; private set; }
        public AStringEventArgs(string theString)
        {
            TheString = theString;
        }
    }
}