﻿using Microsoft.Phone.Shell;
using System;
using System.Windows;
using System.Windows.Controls;

namespace MRTimer
{
    public partial class TimerTriggerSettingControl : UserControl
    {
        const string STATE_KEY_TRIGGER = "TriggerSource";

        EventHandler<TimerCommandEventArgs> handler;
        public void SetHandler(EventHandler<TimerCommandEventArgs> handler)
        {
            this.handler = handler;
        }

        public TimerTriggerSettingControl()
        {
            InitializeComponent();

            object trigger;
            if (PhoneApplicationService.Current.State.TryGetValue(STATE_KEY_TRIGGER, out trigger))
            {
                switch ((TimerCommand)trigger)
                {
                    case TimerCommand.TriggerLED:
                        Radio_LED.IsChecked = true;
                        break;
                    case TimerCommand.TriggerBuzzer:
                        Radio_Buzzer.IsChecked = true;
                        break;
                    case TimerCommand.TriggerBoth:
                        Radio_Both.IsChecked = true;
                        break;
                }
            }
            else
            {
                Radio_LED.IsChecked = true;
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            TimerCommand trigger = TimerCommand.TriggerLED;

            switch ((sender as RadioButton).Name)
            {
                case "Radio_LED":
                    trigger = TimerCommand.TriggerLED;
                    break;
                case "Radio_Buzzer":
                    trigger = TimerCommand.TriggerBuzzer;
                    break;
                case "Radio_Both":
                    trigger = TimerCommand.TriggerBoth;
                    break;
            }

            if (handler != null)
            {
                PhoneApplicationService.Current.State[STATE_KEY_TRIGGER] = trigger;

                handler(this, new TimerCommandEventArgs(TimerCommand.TriggerSource, trigger));
            }
        }
    }
}
