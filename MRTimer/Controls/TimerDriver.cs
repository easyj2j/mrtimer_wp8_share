﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;

namespace MRTimer
{
    // Probably be revising this in v2.
    public enum TimerCommand : byte
    {
        // Standalone commands.
        DoNothing     = 0x0A, // '\n' - useful to clear buffer between commands
        EnableOutput  = 0x6F, // 'o'
        DisableOutput = 0x6E, // 'n'
        LatchDisplay  = 0x64, // 'd'
        EnableDisplay = 0x65, // 'e'

        // The following are preludes and require a subsequent control byte.
        DelayTimeMin    = 0x4E, // 'N'
        DelayTimeMax    = 0x58, // 'X'
            One   = 0x31,
            Two   = 0x32,
            Three = 0x33,
            Four  = 0x34,
            Five  = 0x35,
            Six   = 0x36,
            Seven = 0x37,
            Eight = 0x38,
        TriggerSource   = 0x54, // 'T'
            TriggerLED    = 0x31, // '1'
            TriggerBuzzer = 0x32, // '2'
            TriggerBoth   = 0x33, // '3'
        CompetitionMode = 0x43, // 'C'
            CompetitionOff    = 0x31, // '1'
            CompetitionMaster = 0x32, // '2'
            CompetitionSlave  = 0x33, // '3'
        SpeakerVolume   = 0x56, // 'V', duty cycle in quarters
            VolumeQuarter  = 0x31, // '1'
            VolumeHalf     = 0x32, // '2'
            Volume3Quarter = 0x33, // '3'
            VolumeFull     = 0x34, // '4'
    }

    public static class TimerDriver
    {
        // Offset to convert int to ascii char.
        const int INT_TO_CHAR = 0x30;

        public const TimerCommand DELAY_MIN_MIN = TimerCommand.One;
        public const TimerCommand DELAY_MAX_MAX = TimerCommand.Eight;
        public const TimerCommand DEFAULT_MIN_DELAY = TimerCommand.Two;
        public const TimerCommand DEFAULT_MAX_DELAY = TimerCommand.Seven;

        // Converts a TimerCommand.DelayTimeX to a string representation of an int.
        // ie: TimerCommand.Two returns "2".
        public static string DelayCommandToIntString(TimerCommand command)
        {
            return ((int)command - INT_TO_CHAR).ToString();
        }

        public static async Task SendCommandToTimer(StreamSocket socket, byte[] buffer, Dispatcher _dispatcher)
        {
            Dispatcher dispatcher = _dispatcher;

            if (socket == null)
            {
                return;
            }

            try
            {
                DataWriter writer = new DataWriter(socket.OutputStream);
                writer.WriteBytes(buffer);
                await writer.StoreAsync();

                // TODO: Clear buffer?
            }
            catch (Exception ex)
            {
                SendMessageToUI(dispatcher, ex.Message);
            }
        }

        static void SendMessageToUI(Dispatcher dispatcher, string message)
        {
            dispatcher.BeginInvoke(() =>
            {
                MessageBox.Show(message);
            });
        }
    }
}
