﻿using Microsoft.Phone.Shell;
using System;
using System.Windows;
using System.Windows.Controls;

namespace MRTimer
{
    public partial class TimerCompSettingControl : UserControl
    {
        const string STATE_KEY_COMPETITION = "CompetitionMode";

        EventHandler<TimerCommandEventArgs> handler;
        public void SetHandler(EventHandler<TimerCommandEventArgs> handler)
        {
            this.handler = handler;
        }

        public TimerCompSettingControl()
        {
            InitializeComponent();

            object compMode;
            if (PhoneApplicationService.Current.State.TryGetValue(STATE_KEY_COMPETITION, out compMode))
            {
                switch ((TimerCommand)compMode)
                {
                    case TimerCommand.CompetitionOff:
                        Radio_Off.IsChecked = true;
                        break;
                    case TimerCommand.CompetitionMaster:
                        Radio_Master.IsChecked = true;
                        break;
                    case TimerCommand.CompetitionSlave:
                        Radio_Slave.IsChecked = true;
                        break;
                }
            }
            else
            {
                Radio_Off.IsChecked = true;
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            TimerCommand compMode = TimerCommand.CompetitionOff;

            switch ((sender as RadioButton).Name)
            {
                case "Radio_Off":
                    compMode = TimerCommand.CompetitionOff;
                    break;
                case "Radio_Master":
                    compMode = TimerCommand.CompetitionMaster;
                    break;
                case "Radio_Slave":
                    compMode = TimerCommand.CompetitionSlave;
                    break;
            }

            if (handler != null)
            {
                PhoneApplicationService.Current.State[STATE_KEY_COMPETITION] = compMode;

                handler(this, new TimerCommandEventArgs(TimerCommand.CompetitionMode, compMode));
            }
        }
    }
}
