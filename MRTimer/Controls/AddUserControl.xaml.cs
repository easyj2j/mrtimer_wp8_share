﻿using System;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Controls;
using Windows.Phone.PersonalInformation;

namespace MRTimer
{
    public partial class AddUserControl : UserControl
    {
        EventHandler<NewUserEventArgs> handler;

        public AddUserControl()
        {
            InitializeComponent();
        }

        public void SetHandler(EventHandler<NewUserEventArgs> handler)
        {
            this.handler = handler;
        }

        void UserName_Tap(object sender, EventArgs e)
        {
            UserName.Text = "";
            SubmitName.Content = "Submit";
            SubmitName.IsEnabled = true;
        }

        void SubmitName_Tap(object sender, RoutedEventArgs e)
        {
            if (handler != null)
            {
                handler(this, new NewUserEventArgs(UserName.Text));
            }
            try
            {
                SubmitName.Content = "Saved";
                SubmitName.IsEnabled = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("SubmitName_TapAsync: " + ex.Message);
                SubmitName.Content = "Error";
            }
        }
    }

    public class NewUserEventArgs : EventArgs
    {
        public string Name { get; private set; }
        public NewUserEventArgs(string name)
        {
            Name = name;
        }
    }
}
