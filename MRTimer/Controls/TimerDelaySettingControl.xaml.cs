﻿/*
 * TimerDelaySettingControl.xaml.cs
 * 
 * Sets random delay time range for trigger.
 * Range is between TimerDriver.DELAY_MIN_MIN and TimerDriver.DELAY_MAX_MAX, inclusive.
 * Further restricted to minDelay <= maxDelay.
 */

using Microsoft.Phone.Shell;
using System;
using System.Windows.Controls;

namespace MRTimer
{
    public partial class TimerDelaySettingControl : UserControl
    {
        const string STATE_KEY_MIN_DELAY = "MinDelay";
        const string STATE_KEY_MAX_DELAY = "MaxDelay";

        TimerCommand minDelay, maxDelay;

        EventHandler<TimerCommandEventArgs> handler;
        public void SetHandler(EventHandler<TimerCommandEventArgs> handler)
        {
            this.handler = handler;
        }

        public TimerDelaySettingControl()
        {
            InitializeComponent();

            object min, max;
            minDelay = PhoneApplicationService.Current.State.TryGetValue(STATE_KEY_MIN_DELAY, out min) ? 
                (TimerCommand)min : TimerDriver.DEFAULT_MIN_DELAY;
            maxDelay = PhoneApplicationService.Current.State.TryGetValue(STATE_KEY_MAX_DELAY, out max) ? 
                (TimerCommand)max : TimerDriver.DEFAULT_MAX_DELAY;

            MinDelay.Text = TimerDriver.DelayCommandToIntString(minDelay);
            MaxDelay.Text = TimerDriver.DelayCommandToIntString(maxDelay);
        }

        private void MinDelay_Tap(object sender, EventArgs e)
        {
            Button tapped = sender as Button;

            if("MinDelayDecr" == tapped.Name)
            {
                minDelay--;
                MinDelayIncr.IsEnabled = true;
                MaxDelayDecr.IsEnabled = true;
                if (TimerDriver.DELAY_MIN_MIN == minDelay)
                {
                    tapped.IsEnabled = false;
                }
            }
            else
            {
                minDelay++;
                MinDelayDecr.IsEnabled = true;
                if(minDelay == maxDelay)
                {
                    tapped.IsEnabled = false;
                    MaxDelayDecr.IsEnabled = false;
                }
            }

            MinDelay.Text = TimerDriver.DelayCommandToIntString(minDelay);

            if (handler != null)
            {
                PhoneApplicationService.Current.State[STATE_KEY_MIN_DELAY] = minDelay;

                handler(this, new TimerCommandEventArgs(TimerCommand.DelayTimeMin, minDelay));
            }
        }

        private void MaxDelay_Tap(object sender, EventArgs e)
        {
            Button tapped = sender as Button;

            if ("MaxDelayIncr" == tapped.Name)
            {
                maxDelay++;
                MaxDelayDecr.IsEnabled = true;
                MinDelayIncr.IsEnabled = true;
                if (TimerDriver.DELAY_MAX_MAX == maxDelay)
                {
                    tapped.IsEnabled = false;
                }
            }
            else
            {
                maxDelay--;
                MaxDelayIncr.IsEnabled = true;
                if (minDelay == maxDelay)
                {
                    tapped.IsEnabled = false;
                    MinDelayIncr.IsEnabled = false;
                }
            }

            MaxDelay.Text = TimerDriver.DelayCommandToIntString(maxDelay);

            if (handler != null)
            {
                PhoneApplicationService.Current.State[STATE_KEY_MAX_DELAY] = maxDelay;

                handler(this, new TimerCommandEventArgs(TimerCommand.DelayTimeMax, maxDelay));
            }
        }
    }
}
