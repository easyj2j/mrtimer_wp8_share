﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Networking.Proximity;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;

namespace MRTimer
{
    /*
     * Only dependency is a PeerInformation to connect to.
     */
    public class BluetoothConnection
    {
        // Number of bytes sent by MRTimer per message.
        const uint TIMER_DATABYTE_COUNT = 11;

        const string timerServiceName = "1";

        bool isConnecting;
        bool isConnected;

        // Set by voice commands - corresponding timer commands are sent after successful async connection.
        public bool SendLatch { get; set; }
        public bool ReleaseLatch { get; set; }

        DataReader dataReader;
        DataWriter dataWriter;

        StreamSocket btSocket;

        DataReaderLoadOperation dataReaderLoadOp;

        public event EventHandler ConnectingEvent;
        public event EventHandler<ConnectedEventArgs> ConnectedEvent;
        public event EventHandler DisconnectedEvent;
        public event EventHandler<NewScoreEventArgs> NewScoreEvent;

        public async Task ConnectToTimerAsync(PeerInformation peer)
        {
            // Can't connect to more than 1 device at a time.
            if (btSocket != null)
            {
                // Close socket and release all associated resources.
                CloseSocket();
            }

            try
            {
                btSocket = new StreamSocket();

                //string serviceName = (String.IsNullOrWhiteSpace(peer.ServiceName)) ? tbServiceName.Text : peer.ServiceName;

                // handler == null if no subscribers to event
                EventHandler handlerConnecting = ConnectingEvent;
                if (handlerConnecting != null)
                {
                    // Raise connecting event [using '()' operator] to update UI, etc.
                    handlerConnecting(this, EventArgs.Empty);
                }

                isConnecting = true;
                await btSocket.ConnectAsync(peer.HostName, timerServiceName);//.AsTask().ConfigureAwait(false);

                // No longer on UI thread.
                isConnected = true;
                isConnecting = false;

                EventHandler<ConnectedEventArgs> handlerConnected = ConnectedEvent;
                if (handlerConnected != null)
                {
                    handlerConnected(this, new ConnectedEventArgs(peer.DisplayName));
                }

                dataWriter = new DataWriter(btSocket.OutputStream);

                // Send timer commands corresponding the voice commands.
                if (SendLatch) { SendCommandAsync(TimerCommand.LatchDisplay); }
                if (ReleaseLatch) { SendCommandAsync(TimerCommand.EnableDisplay); }

                await WaitForScoreAsync(btSocket.InputStream);//.ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                // TODO: Catch specific exceptions; unfortunately the docs don't specify the possibilities.
                Debug.WriteLine("ConnectToTimerAsync: " + ex.Message);

                //await SendMessageToUIThread(msg);

                CloseSocket();
            }
        }

        // Waits for scores sent by MRTimer.
        // Scores are made of two times (each a 4 char sequence): reaction & response.
        // Each time is preceded by '&' and the score is appended with '\n'.
        // One score: &motn&resp\n
        async Task WaitForScoreAsync(IInputStream stream)
        {
            dataReader = new DataReader(stream);
            dataReader.InputStreamOptions = InputStreamOptions.Partial;

            EventHandler<NewScoreEventArgs> newScoreHandler = NewScoreEvent;

            SendCommandAsync(TimerCommand.EnableOutput);

            string reaction = string.Empty;
            string motion   = string.Empty;
            string response = string.Empty;

            // Holds chars for reaction or response time
            const int TIME_BUFFER_LENGTH = 4;
            char[] timeBuffer = new char[TIME_BUFFER_LENGTH];
            int index = 0;
            char c, previousChar = '0';

            // Distinguishes between reaction & response times
            bool wasReaction = false;

            // True if both reaction & response times have been read
            bool isScoreComplete = false;

            try
            {
                // Infinitely waits for incoming data unless CloseSocket() is called.
                while (true)
                {
                    // CloseSocket() calls dataReaderLoadOp.Cancel, which cancels
                    // dataReader.LoadAsync while it's waiting for incoming data,
                    // and sets isConnected = false, which terminates the while loop.

                    dataReaderLoadOp = dataReader.LoadAsync(2 * TIMER_DATABYTE_COUNT);
                    await dataReaderLoadOp;//.AsTask().ConfigureAwait(false);

                    if (!isConnected) { break; }    // exit 'while' & WaitForScoreAsync

                    // Reads the incoming buffer and sets reaction & response times.
                    #region  while (dataReader.UnconsumedBufferLength > 0)
                    while (dataReader.UnconsumedBufferLength > 0)
                    {
                        c = (char)dataReader.ReadByte();

                        switch (c)      // Valid input: !, &, 0 - 9, \n
                        {
                            case '!':   // Prefix indicating to zero display -> raise new score event (multiple !'s in a row is an error from timer to be fixed)
                                if ((previousChar != '!') && (newScoreHandler != null))
                                {
                                    newScoreHandler(this, new NewScoreEventArgs(Score.ZERO, Score.ZERO, Score.ZERO));
                                }
                                break;
                            case '&':   // Prefix indicating start of time (reaction or response)
                                break;
                            case '0':   // time char
                            case '1':
                            case '2':
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                            case '7':
                            case '8':
                            case '9':
                                timeBuffer[index++] = c;

                                // Read remaining chars
                                while ((dataReader.UnconsumedBufferLength > 0) && (index < TIME_BUFFER_LENGTH))
                                {
                                    timeBuffer[index++] = (char)dataReader.ReadByte();
                                }

                                if (TIME_BUFFER_LENGTH == index)     // complete reaction or response time read
                                {
                                    index = 0;

                                    if (!wasReaction)
                                    {
                                        // Set reaction string.
                                        reaction = new string(timeBuffer);

                                        wasReaction = true;
                                        isScoreComplete = false;
                                    }
                                    else
                                    {
                                        // Set response string.
                                        response = new string(timeBuffer); ;

                                        wasReaction = false;
                                        isScoreComplete = true;
                                    }
                                }
                                break;
                            case '\n':  // suffix at end of complete score
                                break;
                            default:
                                //await SendMessageToUIThread(msg);
                                Debug.WriteLine("WaitForScoreAsync() invalid char in input stream: {0}", c);
                                break;
                        }
                        previousChar = c;
                    }
                    #endregion

                    if (!isScoreComplete)
                    {
                        // Read more bytes from the incoming buffer.
                        continue;
                    }

                    // Valid complete score

                    if (("9999" == reaction) || ("9999" == response)) // overflow, probably timer just running - not a good score
                    {
                        continue;
                    }

                    if ("0000" == reaction)     // motion switch never pressed
                    {
                        motion = "0000";
                    }
                    else
                    {
                        try
                        {
                            // valid: 1 - 9997
                            motion = (int.Parse(response) - int.Parse(reaction)).ToString().PadLeft(4, '0');
                        }
                        catch (Exception ex)
                        {
                            //await SendMessageToUIThread(msg);
                            Debug.WriteLine("WaitForScoreAsync() bad score reaction/response: {0}/{1}, {2}", 
                                reaction, response, ex.GetType());
                            isScoreComplete = false;
                            continue;
                        }
                    }

                    // Raise new score event.
                    if (newScoreHandler != null)
                    {
                        newScoreHandler(this, 
                            new NewScoreEventArgs(reaction.Insert(1, "."), motion.Insert(1, "."), response.Insert(1, ".")));
                    }

                    isScoreComplete = false;

                    // Repeat for next score.
                }
            }
            catch (Exception ex)
            {
                // TODO: Catch specific exceptions; unfortunately the docs don't specify the possibilities.
                Debug.WriteLine("WaitForScoreAsync: " + ex.Message);

                CloseSocket();
            }
        }

        public async Task SendCommandAsync(TimerCommand timerCommand)
        {
            byte command = (byte)timerCommand;

            if (btSocket == null || isConnected == false)
            {
                //await SendMessageToUIThread(msg);

                Debug.WriteLine("SendCommandAsync: socket not connected");

                return;
            }

            try
            {
                dataWriter.WriteByte(command);
                await dataWriter.StoreAsync();
            }
            catch (Exception ex)
            {
                // TODO: Catch specific exceptions; unfortunately the docs don't specify the possibilities.
                Debug.WriteLine("SendCommandAsync: " + ex.Message);

                CloseSocket();
            }
        }

        void CloseSocket()
        {
            isConnected = false;
            isConnecting = false;

            // Cancel WaitForScoreAsync() dataReader.LoadAsync()
            if (dataReaderLoadOp != null)
            {
                dataReaderLoadOp.Cancel();
                dataReaderLoadOp.Close();
                dataReaderLoadOp = null;
            }

            EventHandler handlerDisconnected = DisconnectedEvent;
            if (handlerDisconnected != null)
            {
                handlerDisconnected(this, EventArgs.Empty);
            }

            if (dataReader != null)
            {
                dataReader.Dispose();
                dataReader = null;
            }
            if (dataWriter != null)
            {
                dataWriter.Dispose();
                dataWriter = null;
            }
            if (btSocket != null)
            {
                btSocket.Dispose();
                btSocket = null;
            }
        }

        public class ConnectedEventArgs : EventArgs
        {
            public string DisplayName { get; private set; }

            public ConnectedEventArgs(string displayName)
            {
                DisplayName = displayName;
            }
        }

        public class NewScoreEventArgs : EventArgs
        {
            public string Reaction { get; private set; }
            public string Motion { get; private set; }
            public string Response { get; private set; }

            public NewScoreEventArgs(string reaction, string motion, string response)
            {
                Reaction = reaction;
                Motion = motion;
                Response = response;
            }
        }
    }
}
