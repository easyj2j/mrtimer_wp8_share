﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input; // for wp8.1 & wsp apps, change to Windows.UI.Input
using System.Windows.Navigation;

namespace MRTimer
{
    public class ScoresViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Score> Scores { get; set; }

        // Set in PopulateScoreListAsync upon successful completion of dbConnection.QueryAsync.
        // TODO: Currently unused.  Just check Scores == null?
        public bool IsDataLoaded { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;
        SQLiteAsyncConnection dbConnection;
        bool isDesc;
        string initialSortColumn = "Inserted";
        static string lastColumnTapped;

        public ScoresViewModel()
        {
            (App.Current as App).ScoresVM = this;

            dbConnection = ((App)App.Current).DbConnection;

            isDesc = true;
        }

        public async Task PopulateScoreListAsync(SQLiteAsyncConnection dbConnection, string orderBy, bool isDesc)
        {
            try
            {
                string direction = isDesc ? " DESC" : string.Empty;
                string sqlQuery = "SELECT * FROM Score ORDER BY " + orderBy + direction;
                await dbConnection.QueryAsync<Score>(sqlQuery, new object[1]).ContinueWith((t) =>  // can throw FileNotFoundException but not caught
                {
                    Scores = new ObservableCollection<Score>(t.Result);                            // can throw AggregateException, which is caught
                    IsDataLoaded = true;
                });

                // Alternative:
                // Create the query (this doesn't connect to DB).
                //var query = dbConnection.Table<Score>().OrderByDescending<DateTime>(v => v.Inserted);
                // Now async connect/query and on return set data binding.
                //Scores = new ObservableCollection<Score>(await query.ToListAsync());
            }
            catch (AggregateException e)
            {
                Debug.WriteLine("PopulateScoreListAsync: " + "AggregateException" + Environment.NewLine + e.Message);
            }
            catch (FileNotFoundException e)
            {
                Debug.WriteLine("PopulateScoreListAsync: " + "fileName not found in install folder." + Environment.NewLine + e.Message);
            }
            catch (SQLiteException ex)
            {
                Debug.WriteLine("PopulateScoreListAsync query.ToListAsync() exception: " + ex.Message);
            }
        }

        public async void SortEventAsync(object sender, SortEventArgs e)
        {
            // Alternate sort direction each consecutive tap on single column.
            // Otherwise, 1st tap ASC except for Inserted column, which is DESC.
            isDesc = (lastColumnTapped == e.SortBy) ? !isDesc : (("Inserted" == e.SortBy) ? true : false);

            await PopulateScoreListAsync(dbConnection, e.SortBy, isDesc);

            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(null));
            }

            lastColumnTapped = e.SortBy;
        }

        public void UpdateScores(Score score)
        {
            Scores.Insert(0, score);
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(null));
            }
        }

        // Handler for horizontal flick, which switches between Score and Scores pages.
        double flickStartX;
        public void Page_FlickStartedEvent(object sender, ManipulationStartedEventArgs e)
        {
            flickStartX = e.ManipulationOrigin.X;
        }
        public void Page_FlickEndedEvent(object sender, ManipulationCompletedEventArgs e)
        {
            double flickDistanceX = Math.Abs(e.ManipulationOrigin.X - flickStartX);

            // Ignore short flicks.
            if (e.IsInertial && (flickDistanceX > 100.0))
            {
                if (sender.GetType().Name != "ScoresPage") { return; }

                Uri scorePage = new Uri("/ScorePage.xaml", UriKind.Relative);

                var journal = new List<JournalEntry>(App.RootFrame.BackStack);

                // Grab ScorePage from backstack if available. This limits backstack to 1 entry (2 when on SettingsPage, 3 if forward nav from app).
                if (journal[journal.Count - 1].Source == scorePage)
                {
                    App.RootFrame.GoBack();
                }
                else
                {
                    App.RootFrame.Navigate(scorePage);
                }
            }
        }
    }
}
