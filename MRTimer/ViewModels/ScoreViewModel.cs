﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input; // for wp8.1 & wsp apps, change to Windows.UI.Input
using Windows.Foundation;
using Windows.Phone.PersonalInformation;

namespace MRTimer
{
    /*
     * Contract for App
     * 
     * ScoreViewModel scoreViewModel
     * bool IsConnected
     * void Timer_Latch(bool latch)
     * void SetLatchPropsForVoice(bool isLatched)
     */
    public class ScoreViewModel : INotifyPropertyChanged
    {
        const string ELLIPSIS = "...";

        App app;
        public ScorePage scoreView { private get; set; } // ref set in ScorePage ctor

        bool isLatched;

        #region Bindings
        public Command LatchTimerCommand { get; private set; }

        ObservableCollection<string> users;
        public ObservableCollection<string> Users
        {
            get { return users; }
            set
            {
                users = value;
                //NotifyPropertyChanged("Users");
            }
        }
        
        string currentUser;
        public string CurrentUser
        {
            get { return currentUser; }
            set
            {
                string temp = value ?? "";

                if (ELLIPSIS == temp)
                {
                    // Limit users to 4 (+ ellipsis) due to ListPicker switching to full screen for more.
                    if (users.Count >= 5)
                    {
                        scoreView.UserList.SelectedItem = currentUser;
                    }
                    else
                    {
                        scoreView.UserList.Visibility = Visibility.Collapsed;
                        scoreView.AddUserControls.Visibility = Visibility.Visible;
                        scoreView.UserList.SelectedItem = currentUser;
                    }
                }
                else
                {
                    currentUser = temp;
                    LatestScore = new Score(currentUser);
                    NotifyPropertyChanged("CurrentUser");
                }
            }
        }
        
        Score latestScore;
        public Score LatestScore
        {
            get { return latestScore; }
            set
            {
                latestScore = value;
                NotifyPropertyChanged("LatestScore");
                IsolatedStorageSettings.ApplicationSettings["LatestScore"] = latestScore;
            }
        }
       
        bool isConnected;
        public bool IsConnected
        {
            get { return isConnected; }
            set
            {
                isConnected = value;
                NotifyPropertyChanged("IsConnected");
                LatchTimerCommand.RaiseCanExecuteChanged();
            }
        }
        #endregion


        public ScoreViewModel()
        {
            app = (App)App.Current;

            users = new ObservableCollection<string>();
            PopulateUserListAsync();

            LatchTimerCommand = new Command(CanLatchTimer, LatchTimer);

            IsConnected = app.IsConnected;
        }

        // Populates UserList with app contacts. 
        // Sets CurrentUser to either (in order): 
        //      user associated w last stored score, 
        //      UserList[0] if no stored score, 
        //      ELLIPSIS if no contacts (ELLIPSIS added as UserList[0]).
        public async Task PopulateUserListAsync()
        {
            try
            {
                // Retrieve app users from contacts.
                ContactStore store = await ContactStore.CreateOrOpenAsync(
                    ContactStoreSystemAccessMode.ReadOnly, ContactStoreApplicationAccessMode.ReadOnly);

                //store.DeleteAsync();

                IReadOnlyList<StoredContact> contacts = await store.CreateContactQuery().GetContactsAsync();

                if ((null == contacts) || (contacts.Count == 0))
                {
                    // No app users
                    users.Add("...");
                    scoreView.UserList.SelectedItem = ELLIPSIS;
                    CurrentUser = string.Empty;
                    LatestScore = new Score();
                    return;
                }
                else
                {
                    Score tempScore;

                    // Retrieve latest score from storage.
                    if (!IsolatedStorageSettings.ApplicationSettings.TryGetValue<Score>(
                        "LatestScore", out tempScore) || (null == tempScore))
                    {
                        // No stored score
                        tempScore = new Score();
                    }

                    bool currentUserFound = false;
                    foreach (StoredContact contact in contacts)
                    {
                        Users.Add(contact.DisplayName);
                        Debug.WriteLine("Added user {{ID}}: {0} {1}", contact.DisplayName, contact.Id);
                        if (contact.DisplayName == tempScore.Name)
                        {
                            currentUserFound = true;
                        }
                    }
                    users.Add(ELLIPSIS);
                    Debug.WriteLine("Added user {{ID}}: {0} {1}", ELLIPSIS, "");
                    CurrentUser = currentUserFound ? tempScore.Name : Users[0];
                    LatestScore = tempScore;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("PopulateUserListAsync: " + e.Message);
            }
        }

        public async Task UserSubmitName_TapAsync(object sender, EventArgs e)
        {
            try
            {
                ContactStore store = await ContactStore.CreateOrOpenAsync(
                    ContactStoreSystemAccessMode.ReadOnly, ContactStoreApplicationAccessMode.ReadOnly);

                StoredContact contact = new StoredContact(store);
                contact.DisplayName = scoreView.UserName.Text;

                try
                {
                    await contact.SaveAsync();

                    IsolatedStorageSettings.ApplicationSettings.Add(contact.DisplayName, contact.Id);
                    Debug.WriteLine("New user {{ID}}: {0} {1}", contact.DisplayName, contact.Id);

                    if(null != users)
                    {
                        // TODO: sort list
                        users.Insert(users.Count - 1, scoreView.UserName.Text);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("UserSubmitName_TapAsync: exception from SaveAsync\n{0}", ex.ToString());
                }

                scoreView.UserName.Text = "Enter name";
                scoreView.UserSubmitName.Content = "Saved";
                scoreView.UserSubmitName.IsEnabled = false;

                // Limit users to 4 (+ ellipsis) due to ListPicker switching to full screen for more.
                if(users.Count >= 5)
                {
                    scoreView.UserDone_Tap(sender, e);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("SubmitName_TapAsync: " + ex.Message);
                scoreView.UserSubmitName.Content = "Error";
            }
        }

        public async Task UserDelete_ClickAsync(object sender, RoutedEventArgs e)
        {
            string id;
            string name = (sender as MenuItem).DataContext as string;

            try
            {
                if ((IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>(name, out id)) && (null != id))
                {
                    ContactStore store = await ContactStore.CreateOrOpenAsync(
                        ContactStoreSystemAccessMode.ReadOnly, ContactStoreApplicationAccessMode.ReadOnly);

                    try
                    {
                        await store.DeleteContactAsync(id);

                        Debug.WriteLine("User deleted {{ID}}: {0} {1}", name, id);

                        IsolatedStorageSettings.ApplicationSettings.Remove(name);
                        users.Remove(name);
                        if(name == currentUser)
                        {
                            CurrentUser = Users[0];
                        }
                    }
                    catch(Exception ex)
                    {
                        Debug.WriteLine("UserDelete_ClickAsync: exception from DeleteContactAsync\n{0}", ex.ToString());
                    }
                }
                else
                {
                    Debug.WriteLine("UserDelete_ClickAsync: unable to retrieve user from Isolated Storage: {0}", name);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("UserDelete_ClickAsync: " + ex.Message);
            }
        }

        public async Task UserEditName_ClickAsync(object sender, RoutedEventArgs e)
        {
            string id;
            string currentName = (sender as MenuItem).DataContext as string;

            // TODO: Need a popup w a TextBox to get newName.
            string newName = "B Leeee";

            try
            {
                if ((IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>(currentName, out id)) && (null != id))
                {
                    ContactStore store = await ContactStore.CreateOrOpenAsync(
                        ContactStoreSystemAccessMode.ReadOnly, ContactStoreApplicationAccessMode.ReadOnly);

                    StoredContact contact = await store.FindContactByIdAsync(id);
                    contact.DisplayName = newName;

                    await contact.SaveAsync();  // ReplaceExistingContactAsync(id);
                    IsolatedStorageSettings.ApplicationSettings[currentName] = newName;
                    users[users.IndexOf(currentName)] = newName;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("UserEditName_ClickAsync: " + ex.Message);
            }
        }

        bool CanLatchTimer(object parameter)
        {
            return IsConnected;
        }

        void LatchTimer(object parameter)
        {
            // Toggle timer display: latched/running.
            isLatched = !isLatched;

            app.Timer_Latch(isLatched);

            UpdateLatchButtonState();
        }

        public void UpdateLatchButtonState()
        {
            scoreView.UpdateLatchButtonState(isLatched);
        }

        public void UpdateScore(Score score)
        {
            LatestScore = Score.CopyScore(score);
        }

        public void CheckForVoiceCommand()
        {
            /*
             * Determine if arrived here through voice command.  If so, try to comply.
             * 
             *  For app targeting WP8.0 (regardless of OS):
             *  When system voice command process returns, BT connection is killed and MRTimer app recreated.
             *  Attempting to write directly to timer doesn't succeed as async connection attempt hasn't finished.
             *  Instead set bool on btConnection (via app), then btConnection checks bool after connection has successfully completed.
             *  
             *  For app targeting WP8.1 Silverlight: App/BT connection not killed so can write straight to timer.
             *  Reverted back to WP8.0 from upgraded WP8.1 SL app because BT connection was flaky.
             */
            string voiceCommand;
            bool setByVoice = scoreView.NavigationContext.QueryString.TryGetValue("voiceCommandName", out voiceCommand);

            if (!setByVoice || (null == voiceCommand)) { return; }

            // Currently only 'latch' & 'enable' commands.  If this changes, so too must the following.
#if WP8_0
            app.SetLatchPropsForVoice("LatchTimer" == voiceCommand);
#else
            app.Timer_Latch("LatchTimer" == voiceCommand);
#endif
        }

        // Handler for horizontal flick, which switches between Score and Scores pages.
        double flickStartX;
        public void Page_FlickStartedEvent(object sender, ManipulationStartedEventArgs e)
        {
            flickStartX = e.ManipulationOrigin.X;
        }
        public void Page_FlickEndedEvent(object sender, ManipulationCompletedEventArgs e)
        {
            double flickDistanceX = Math.Abs(e.ManipulationOrigin.X - flickStartX);

            // Ignore short flicks
            if (e.IsInertial && (flickDistanceX > 100.0))
            {
                if (sender.GetType().Name != "ScorePage") { return; }

                // Navigate to ScoresPage.
                App.RootFrame.Navigate(new Uri("/ScoresPage.xaml", UriKind.Relative));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
