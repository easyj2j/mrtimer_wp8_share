﻿using Microsoft.Phone.Controls;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using Windows.UI;

namespace MRTimer
{
    /*
     * Contract for ViewModel
     * 
     * Data bindings:
     * ObservableCollection<string> Users
     * string CurrentUser
     * Score LatestScore
     * Command LatchTimerCommand
     * 
     * bool IsConnected
     * void RePopulateUserListAsync()
     * void CheckForVoiceCommand()
     */
    public partial class ScorePage : PhoneApplicationPage
    {
        const string LATCHED = "Latch";
        const string RESET = "Reset";
        const string NOTCONNECTED = "Not Connected";

        ScoreViewModel scoreViewModel;

        // Set in UserMenu_Opened; checked & null'ed in OnBackKeyPress
        ContextMenu menu;


        public ScorePage()
        {
            InitializeComponent();

            scoreViewModel = (App.Current as App).ScoreVM;

            DataContext = scoreViewModel;

            scoreViewModel.scoreView = this;

            ManipulationStarted += scoreViewModel.Page_FlickStartedEvent;
            ManipulationCompleted += scoreViewModel.Page_FlickEndedEvent;

            UpdateLatchButtonState(false);
        }

        // Button text displays "Reset" when timer is latched.
        public void UpdateLatchButtonState(bool isLatched)
        {
            if (!scoreViewModel.IsConnected)
            {
                LatchButton.Content = NOTCONNECTED;
                return;
            }
            else
            {
                LatchButton.Content = isLatched ? RESET : LATCHED;
            }
        }

        void UserName_Tap(object sender, EventArgs e)
        {
            UserName.Text = "";
            UserSubmitName.Content = "Submit";
            UserSubmitName.IsEnabled = true;
            UserDone.IsEnabled = true;
        }

        async void UserSubmitName_TapAsync(object sender, EventArgs e)
        {
            await scoreViewModel.UserSubmitName_TapAsync(sender, e);
        }

        public void UserDone_Tap(object sender, EventArgs e)
        {
            UserName.Text = "Enter name";
            UserSubmitName.Content = "Submit";
            UserSubmitName.IsEnabled = false;
            UserDone.IsEnabled = false;

            UserList.Visibility = Visibility.Visible;
            AddUserControls.Visibility = Visibility.Collapsed;
        }

        async void UserDelete_ClickAsync(object sender, RoutedEventArgs e)
        {
            await scoreViewModel.UserDelete_ClickAsync(sender, e);
        }

        async void UserEditName_ClickAsync(object sender, RoutedEventArgs e)
        {
            //await scoreViewModel.UserEditName_ClickAsync(sender, e);
        }

        private void UserMenu_Opened(object sender, RoutedEventArgs e)
        {
            menu = sender as ContextMenu;

            //(menu.Owner as TextBlock).FontStyle = FontStyles.Italic;

            string name = ((sender as FrameworkElement).DataContext as string) ?? "";
            (menu.Items[0] as MenuItem).Header = "Delete " + name;
        }

        //private void UserMenu_Closed(object sender, RoutedEventArgs e)
        //{
        //    if (menu != null)
        //    {
        //        menu = sender as ContextMenu;
        //        (menu.Owner as TextBlock).FontStyle = FontStyles.Normal;
        //        menu = null;
        //    }
        //}

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            // ContextMenu to delete/edit user
            if ((menu != null) && menu.IsOpen)
            {
                menu.IsOpen = false;
                menu = null;
                e.Cancel = true;
            }
            // Add user
            else if(Visibility.Visible == AddUserControls.Visibility)
            {
                UserDone_Tap(null, null);
                e.Cancel = true;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // Determine if navigated here by voice command; if so, try to comply.
            scoreViewModel.CheckForVoiceCommand();

            // For WP8.0 app (either WP OS 8.0 or 8.1), if arrived via voice -> IsConnected == false.
            // For WP8.1 SL, if arrived through voice -> IsConnected remains at previous state.
            if (!scoreViewModel.IsConnected) { LatchButton.Content = NOTCONNECTED; }
        }
    }
}