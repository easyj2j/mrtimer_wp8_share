using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using MRTimer.Resources;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Windows.Networking.Proximity;

namespace MRTimer
{
    public partial class TimerSettingsPage : PhoneApplicationPage
    {
        public event EventHandler<TimerCommandEventArgs> TimerCommandEvent;

        // Local copy of paired device information.
        ObservableCollection<PairedDeviceInfo> _pairedDevices;

       
        public TimerSettingsPage()
        {
            InitializeComponent();

            App app = (App)App.Current;
            app.TimerSettings_Page = this;
            TimerCommandEvent += app.TimerSettings_TimerCommandEvent;

            TriggerControl.SetHandler(TimerCommandEvent);
            DelayControl.SetHandler(TimerCommandEvent);
            CompetitionControl.SetHandler(TimerCommandEvent);

            Loaded += SettingsPage_Loaded;
        }

        void SettingsPage_Loaded(object sender, RoutedEventArgs e)
        {
            // Bluetooth is not available in the emulator. 
            if (Microsoft.Devices.Environment.DeviceType == Microsoft.Devices.DeviceType.Emulator)
            {
                MessageBox.Show(AppResources.Msg_EmulatorMode,"Warning",MessageBoxButton.OK);
            }

            _pairedDevices = new ObservableCollection<PairedDeviceInfo>();
            PairedDevicesList.ItemsSource = _pairedDevices;
        }

        void SelectTimer_Tap(object sender, EventArgs e)
        {
            if (!PairedDevicesList.IsEnabled)
            {
                RefreshPairedDevicesListAsync();
            }
            else
            {
                _pairedDevices.Clear();
                PairedDevicesList.IsEnabled = false;
                ConnectToSelected.Visibility = Visibility.Collapsed;
                ConnectToSelected.IsEnabled = false;
            }
        }

        /// <summary>
        /// Asynchronous call to re-populate the ListBox of paired devices.
        /// </summary>
        async Task RefreshPairedDevicesListAsync()
        {
            try
            {
                // Search for all paired devices
                PeerFinder.AlternateIdentities["Bluetooth:Paired"] = "";
                var peers = await PeerFinder.FindAllPeersAsync();

                _pairedDevices.Clear();

                if (peers.Count == 0)
                {
                    MessageBox.Show(AppResources.Msg_NoPairedDevices);
                }
                else
                {
                    foreach (var peer in peers)
                    {
                        // Filter for MRTimer devices
                        if (!peer.DisplayName.StartsWith("MRTimer")) { continue; }

                        _pairedDevices.Add(new PairedDeviceInfo(peer));
                    }
                    PairedDevicesList.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                if ((uint)ex.HResult == 0x8007048F)
                {
                    var result = MessageBox.Show(AppResources.Msg_BluetoothOff, "Bluetooth Off", MessageBoxButton.OKCancel);
                    if (result == MessageBoxResult.OK)
                    {
                        ShowBluetoothControlPanel();
                    }
                }
                else if ((uint)ex.HResult == 0x80070005)
                {
                    MessageBox.Show(AppResources.Msg_MissingCaps);
                }
                else
                {
                    MessageBox.Show("RefreshPairedDevicesListAsync: " + ex.Message);
                }
            }
        }

        void PairedDevicesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PairedDevicesList.SelectedItem == null)
            {
                ConnectToSelected.IsEnabled = false;
                //ServiceNameInput.Visibility = Visibility.Collapsed;
            }
            else
            {
                ConnectToSelected.IsEnabled = true;
                ConnectToSelected.Visibility = Visibility.Visible;
                ConnectToSelected.Content = "Connect";

                PairedDeviceInfo pdi = PairedDevicesList.SelectedItem as PairedDeviceInfo;
                // Show the service name field, if the ServiceName associated with this device is currently empty.
                //ServiceNameInput.Visibility = (String.IsNullOrWhiteSpace(pdi.ServiceName)) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        async void ConnectToSelected_TapAsync(object sender, EventArgs e)
        {
            PairedDeviceInfo pdi = PairedDevicesList.SelectedItem as PairedDeviceInfo;
            PeerInformation peer = pdi.PeerInfo;

            // Async call to connect to device
            await ((App)App.Current).Timer_ConnectAsync(peer).ConfigureAwait(false);
        }

        void ShowBluetoothControlPanel()
        {
            ConnectionSettingsTask connectionSettingsTask = new ConnectionSettingsTask();
            connectionSettingsTask.ConnectionSettingsType = ConnectionSettingsType.Bluetooth;
            connectionSettingsTask.Show();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            App app = (App)App.Current;

            if (app.IsConnected)
            {
                TriggerControl.IsEnabled = true;
                DelayControl.IsEnabled = true;
                CompetitionControl.IsEnabled = true;
                AppTitle.Text = "Connected to " + app.AppTitle;
            }
        }
    }

    public class TimerCommandEventArgs : EventArgs
    {
        public TimerCommand Category { get; private set; }
        public TimerCommand Command { get; private set; }

        public TimerCommandEventArgs(TimerCommand category, TimerCommand command)
        {
            Category = category;
            Command = command;
        }
    }

    public class PairedDeviceInfo
    {
        public string DisplayName { get; private set; }
        public string HostName { get; private set; }
        public string ServiceName { get; private set; }
        public PeerInformation PeerInfo { get; private set; }

        internal PairedDeviceInfo(PeerInformation peerInformation)
        {
            this.PeerInfo = peerInformation;
            this.DisplayName = this.PeerInfo.DisplayName;
            this.HostName = this.PeerInfo.HostName.DisplayName;
            this.ServiceName = this.PeerInfo.ServiceName;
        }
    }
}
