﻿using Microsoft.Phone.Controls;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace MRTimer
{
    public partial class ScoresPage : PhoneApplicationPage
    {
        const int insertedHeaderColumnIndex = 6;

        public event EventHandler<SortEventArgs> SortEvent;

        TextBlock italicizeBlock;


        public ScoresPage()
        {
            InitializeComponent();

            App app = App.Current as App;
            ManipulationStarted += app.ScoresVM.Page_FlickStartedEvent;
            ManipulationCompleted += app.ScoresVM.Page_FlickEndedEvent;

            DataContext = app.ScoresVM;

            SortEvent += app.ScoresVM.SortEventAsync;
            app.ScoresVM.PropertyChanged += PropertyChanged;
            //ScoresList.ItemsSource = app.ScoresVM.Scores;

            // dummy - just need this initialized for ColumnTap
            italicizeBlock = HName;     
        }

        // Sorts the scores by the tapped column and 
        // italicizes the corresponding header.
        void Column_Tap(object sender, RoutedEventArgs e)
        {
            string sortColumn;

            // Clear previous italicized header.
            italicizeBlock.FontStyle = FontStyles.Normal;

            TextBlock block = sender as TextBlock;
            string elementName = block.Name;

            if (elementName.StartsWith("H"))
            {
                sortColumn = elementName.Substring(1);
                italicizeBlock = block;
            }
            else
            {
                sortColumn = elementName;
                italicizeBlock = FindName("H" + elementName) as TextBlock;
            }

            // Italicize header corresponding to sort column.
            italicizeBlock.FontStyle = FontStyles.Italic;

            // Hack using TitlePanel to sort on invisible column when in Portrait mode.
            sortColumn = ("AppTitle" == sortColumn) ? "Inserted" : sortColumn;

            if (SortEvent != null)
            {
                SortEvent(this, new SortEventArgs(sortColumn));
            }
        }

        void ScoresPage_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            if ((e.Orientation & PageOrientation.Portrait) == PageOrientation.Portrait)
            {
                ScoresList.Width = 430;
                LayoutRoot.Margin = new Thickness(0);
                Header.ColumnDefinitions[0].Width = new GridLength(1.1, GridUnitType.Star);   // Name
                Header.ColumnDefinitions[insertedHeaderColumnIndex].Width = new GridLength(0);
                HInserted.Visibility = System.Windows.Visibility.Collapsed;
                ScoresList.ItemTemplate = Resources["ScoresTemplate"] as DataTemplate;
            }
            else    // switched to landscape
            {
                ScoresList.Width = 640;
                LayoutRoot.Margin = (e.Orientation == PageOrientation.LandscapeRight) ? new Thickness(0, 0, -72, 0) : new Thickness(0);
                Header.ColumnDefinitions[0].Width = new GridLength(1, GridUnitType.Star);     // Name
                Header.ColumnDefinitions[insertedHeaderColumnIndex].Width = new GridLength(2.6, GridUnitType.Star);
                HInserted.Visibility = System.Windows.Visibility.Visible;
                ScoresList.ItemTemplate = Resources["ScoresTemplate_Landscape"] as DataTemplate;
            }
        }

        void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //ScoresList.ItemsSource = (App.Current as App).ScoresVM.Scores;
            ScoresList.ScrollTo(ScoresList.ItemsSource[0]);
        }
    }

    public class SortEventArgs : EventArgs
    {
        public string SortBy { get; private set; }
        public SortEventArgs(string sortBy)
        {
            SortBy = sortBy;
        }
    }
}