﻿using System.Windows;
using System.Windows.Controls;

namespace MRTimer
{
    public partial class DeleteUserPopup : UserControl
    {
        public string UserName { get; set; }

        public DeleteUserPopup()
        {
            InitializeComponent();
        }
    }
}
