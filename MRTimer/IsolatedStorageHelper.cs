﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows;

namespace MRTimer
{
    public class IsolatedStorageHelper
    {
        // If the specified file exists in the install folder but not the local folder, copy it there.
        // Files in install folder are read-only, while those in local folder are RW.
        public static void CopyFileFromInstallToLocalFolder(string fileName, FileMode mode)
        {
            try
            {
                // Get an input stream for the install folder file.  Throws IOException if file doesn't exists.
                using (Stream input = Application.GetResourceStream(new Uri(fileName, UriKind.Relative)).Stream)
                // Get local storage.
                using (IsolatedStorageFile iso = IsolatedStorageFile.GetUserStoreForApplication())
                // Get an output stream for the local folder file.
                using (IsolatedStorageFileStream output = new IsolatedStorageFileStream(fileName, mode, iso))
                {
                    byte[] readBuffer = new byte[4096];
                    int bytesRead = -1;

                    // Copy the file from the installation folder to the local folder. 
                    while ((bytesRead = input.Read(readBuffer, 0, readBuffer.Length)) > 0)
                    {
                        output.Write(readBuffer, 0, bytesRead);
                    }
                }
            }
            catch (IOException e)
            {
                Debug.WriteLine("IsolatedStorageHelper: " + fileName + " not found in install folder." + Environment.NewLine + e.Message);
            }
            catch (IsolatedStorageException e)
            {
                Debug.WriteLine("IsolatedStorageHelper: " + fileName + Environment.NewLine + e.Message);
            }
        }
    }
}
