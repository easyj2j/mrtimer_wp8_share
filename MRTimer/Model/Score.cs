﻿using SQLite;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MRTimer
{
    /*
     * Represents a timer device 'score' in a DB.
     * Reaction, Motion, & Response are sent by the device over BT.
     * Inserted is specified when the score in inserted into the DB, which sets the ID.
     * Uploaded is specified when the score is uploaded to the server; currently unimplemented.
     */
    public class Score : INotifyPropertyChanged
    {
        // Consts are accessed as static members.
        public const string ZERO = "0.000";

        public event PropertyChangedEventHandler PropertyChanged;

        // Can't use auto-implemented property when calling NotifyPropertyChanged.
        // -> Name & Response are the only properties that when changed -> trigger PropertyChanged event.

        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        string name;
        [MaxLength(32)]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                NotifyPropertyChanged();
            }
        }
        [MaxLength(5)]
        public string Reaction { get; set; }
        [MaxLength(5)]
        public string Motion { get; set; }
        string response;
        [MaxLength(5)]
        public string Response 
        {
            get
            {
                return response;
            }
            set
            {
                response = value;
                NotifyPropertyChanged();
            }
        }
        public DateTime Inserted { get; set; }
        public DateTime Uploaded { get; set; }

        // Necessary for use with SQLite generics.
        public Score() : this("", ZERO, ZERO, ZERO) { }

        public Score(string name) : this(name, ZERO, ZERO, ZERO) { }

        public Score(string name, string reaction, string motion, string response) :
                this(name, reaction, motion, response, DateTime.Now, DateTime.MinValue) {}

        public Score(string name, string reaction, string motion, string response, DateTime inserted, DateTime uploaded)
        {
            this.Name = name ?? "";
            this.Reaction = reaction;
            this.Motion = motion;
            this.Response = response;
            this.Inserted = inserted;
            this.Uploaded = uploaded;
        }

        public static Score CopyScore(Score score)
        {
            return new Score(score.Name, score.Reaction, score.Motion, score.Response);
        }

        // When CallerMemberName attribute is specified -> propertyName doesn't have to be specified.
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                // null specifies that all properties have changed.
                PropertyChanged(this, new PropertyChangedEventArgs(null));
            }
        }
    }
}
